# pvValidator

Tool to validate EPICS PVs based on CHESS document [ESS-0000757](https://chess.esss.lu.se/enovia/link/ESS-0000757/21308.51166.43264.12914/valid).

**On Centos7 the installation is only via a pre-compiled pip package**

Requirements: `Python3` and `pip3`

`pip3 install pvValidatorUtils -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple --user`

**Platform supported**

Linux, WSL

**How to run**

Then you can run the CLI **pvValidator**
```
 $ pvValidator -h
usage: pvValidator [-h] [-v] [-d] (-s (IP[:PORT] | GUID) | -i pvfile | -e dbfile [VAR=VALUE ...] | -m subsfile
                   [path_to_templates VAR=VALUE ...]) [-n {prod,test} | --noapi] [-o csvfile | --stdout]

EPICS PV Validation Tool (1.8.0)

options:
  -h, --help            show this help message and exit
  -v, --version         print version and exit
  -d, --discover        discover IOC servers and exit
  -s (IP[:PORT] | GUID)
                        IOC server IP[:PORT] or GUID to get PV list (online validation)
  -i pvfile             input PV list file (offline validation)
  -e dbfile [VAR=VALUE ...]
                        input EPICS db file (.db) [VAR=VALUE, ...] (offline validation)
  -m subsfile [path_to_templates VAR=VALUE ...]
                        input substitution file (.substitutions) [path_to_templates VAR=VALUE, ...] (offline validation)
  -n {prod,test}        select naming service endpoint to connect: prod(uction), test(ing) [default=prod]
  --noapi               check only PV format and rules, skip connection to naming service endpoint
  -o csvfile            write the validation table directly to CSV file
  --stdout              write the validation table directly to STDOUT

Copyright 2021 - Alfio Rizzo (alfio.rizzo@ess.eu)
```

## Author
Alfio Rizzo (alfio.rizzo@ess.eu)

## Acknowledgment

EPICS https://epics-controls.org

## License
GNU GENERAL PUBLIC LICENSE, Version 3, 29 June 2007
