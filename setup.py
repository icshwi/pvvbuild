from setuptools import find_packages, setup

setup(
    name="pvValidatorUtils",
    packages=find_packages(),
    package_data={"": ["*.so*"]},
    version="1.8.0",
    description="pvValidator.py is a tool to validate EPICS PVs based on CHESS document [ESS-0000757]",
    author="Alfio Rizzo",
    author_email="alfio.rizzo@ess.eu",
    license="GPL",
    zip_safe=False,
    platforms=["Linux", "WSL"],
    scripts=["bin/pvValidator"],
    install_requires=["requests"],
)
