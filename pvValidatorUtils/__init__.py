from ctypes import cdll

import pkg_resources

loc = pkg_resources.require("pvValidatorUtils")[0].location

libs = loc + "/pvValidatorUtils/"


cdll.LoadLibrary(libs + "libCom.so.3.23.1")
cdll.LoadLibrary(libs + "libca.so.4.14.4")
cdll.LoadLibrary(libs + "libpvData.so.8.0.6")
cdll.LoadLibrary(libs + "libpvAccess.so.7.1.7")
cdll.LoadLibrary(libs + "libpvAccessCA.so.7.1.7")

cdll.LoadLibrary(libs + "_epicsUtils.so")
cdll.LoadLibrary(libs + "_msiUtils.so")
from .epicsUtils import epicsUtils  # noqa
from .pvUtils import pvUtils  # noqa

version = pkg_resources.require("pvValidatorUtils")[0].version
