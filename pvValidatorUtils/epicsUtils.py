# This file was automatically generated by SWIG (http://www.swig.org).
# Version 2.0.10
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.


from sys import version_info

if version_info >= (2, 6, 0):

    def swig_import_helper():
        import imp
        from os.path import dirname

        fp = None
        try:
            fp, pathname, description = imp.find_module(
                "_epicsUtils", [dirname(__file__)]
            )
        except ImportError:
            import _epicsUtils

            return _epicsUtils
        if fp is not None:
            try:
                _mod = imp.load_module("_epicsUtils", fp, pathname, description)
            finally:
                fp.close()
            return _mod

    _epicsUtils = swig_import_helper()
    del swig_import_helper
else:
    import _epicsUtils
del version_info
try:
    _swig_property = property
except NameError:
    pass  # Python < 2.2 doesn't have 'property'.


def _swig_setattr_nondynamic(self, class_type, name, value, static=1):
    if name == "thisown":
        return self.this.own(value)
    if name == "this":
        if type(value).__name__ == "SwigPyObject":
            self.__dict__[name] = value
            return
    method = class_type.__swig_setmethods__.get(name, None)
    if method:
        return method(self, value)
    if not static:
        self.__dict__[name] = value
    else:
        raise AttributeError("You cannot add attributes to %s" % self)


def _swig_setattr(self, class_type, name, value):
    return _swig_setattr_nondynamic(self, class_type, name, value, 0)


def _swig_getattr(self, class_type, name):
    if name == "thisown":
        return self.this.own()
    method = class_type.__swig_getmethods__.get(name, None)
    if method:
        return method(self)
    raise AttributeError(name)


def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except:
        strthis = ""
    return "<%s.%s; %s >" % (
        self.__class__.__module__,
        self.__class__.__name__,
        strthis,
    )


try:
    _object = object
    _newclass = 1
except AttributeError:

    class _object:
        pass

    _newclass = 0


class SwigPyIterator(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(
        self, SwigPyIterator, name, value
    )
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, SwigPyIterator, name)

    def __init__(self, *args, **kwargs):
        raise AttributeError("No constructor defined - class is abstract")

    __repr__ = _swig_repr
    __swig_destroy__ = _epicsUtils.delete_SwigPyIterator
    __del__ = lambda self: None

    def value(self):
        return _epicsUtils.SwigPyIterator_value(self)

    def incr(self, n=1):
        return _epicsUtils.SwigPyIterator_incr(self, n)

    def decr(self, n=1):
        return _epicsUtils.SwigPyIterator_decr(self, n)

    def distance(self, *args):
        return _epicsUtils.SwigPyIterator_distance(self, *args)

    def equal(self, *args):
        return _epicsUtils.SwigPyIterator_equal(self, *args)

    def copy(self):
        return _epicsUtils.SwigPyIterator_copy(self)

    def next(self):
        return _epicsUtils.SwigPyIterator_next(self)

    def __next__(self):
        return _epicsUtils.SwigPyIterator___next__(self)

    def previous(self):
        return _epicsUtils.SwigPyIterator_previous(self)

    def advance(self, *args):
        return _epicsUtils.SwigPyIterator_advance(self, *args)

    def __eq__(self, *args):
        return _epicsUtils.SwigPyIterator___eq__(self, *args)

    def __ne__(self, *args):
        return _epicsUtils.SwigPyIterator___ne__(self, *args)

    def __iadd__(self, *args):
        return _epicsUtils.SwigPyIterator___iadd__(self, *args)

    def __isub__(self, *args):
        return _epicsUtils.SwigPyIterator___isub__(self, *args)

    def __add__(self, *args):
        return _epicsUtils.SwigPyIterator___add__(self, *args)

    def __sub__(self, *args):
        return _epicsUtils.SwigPyIterator___sub__(self, *args)

    def __iter__(self):
        return self


SwigPyIterator_swigregister = _epicsUtils.SwigPyIterator_swigregister
SwigPyIterator_swigregister(SwigPyIterator)


class VectorInt(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, VectorInt, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, VectorInt, name)
    __repr__ = _swig_repr

    def iterator(self):
        return _epicsUtils.VectorInt_iterator(self)

    def __iter__(self):
        return self.iterator()

    def __nonzero__(self):
        return _epicsUtils.VectorInt___nonzero__(self)

    def __bool__(self):
        return _epicsUtils.VectorInt___bool__(self)

    def __len__(self):
        return _epicsUtils.VectorInt___len__(self)

    def pop(self):
        return _epicsUtils.VectorInt_pop(self)

    def __getslice__(self, *args):
        return _epicsUtils.VectorInt___getslice__(self, *args)

    def __setslice__(self, *args):
        return _epicsUtils.VectorInt___setslice__(self, *args)

    def __delslice__(self, *args):
        return _epicsUtils.VectorInt___delslice__(self, *args)

    def __delitem__(self, *args):
        return _epicsUtils.VectorInt___delitem__(self, *args)

    def __getitem__(self, *args):
        return _epicsUtils.VectorInt___getitem__(self, *args)

    def __setitem__(self, *args):
        return _epicsUtils.VectorInt___setitem__(self, *args)

    def append(self, *args):
        return _epicsUtils.VectorInt_append(self, *args)

    def empty(self):
        return _epicsUtils.VectorInt_empty(self)

    def size(self):
        return _epicsUtils.VectorInt_size(self)

    def clear(self):
        return _epicsUtils.VectorInt_clear(self)

    def swap(self, *args):
        return _epicsUtils.VectorInt_swap(self, *args)

    def get_allocator(self):
        return _epicsUtils.VectorInt_get_allocator(self)

    def begin(self):
        return _epicsUtils.VectorInt_begin(self)

    def end(self):
        return _epicsUtils.VectorInt_end(self)

    def rbegin(self):
        return _epicsUtils.VectorInt_rbegin(self)

    def rend(self):
        return _epicsUtils.VectorInt_rend(self)

    def pop_back(self):
        return _epicsUtils.VectorInt_pop_back(self)

    def erase(self, *args):
        return _epicsUtils.VectorInt_erase(self, *args)

    def __init__(self, *args):
        this = _epicsUtils.new_VectorInt(*args)
        try:
            self.this.append(this)
        except:
            self.this = this

    def push_back(self, *args):
        return _epicsUtils.VectorInt_push_back(self, *args)

    def front(self):
        return _epicsUtils.VectorInt_front(self)

    def back(self):
        return _epicsUtils.VectorInt_back(self)

    def assign(self, *args):
        return _epicsUtils.VectorInt_assign(self, *args)

    def resize(self, *args):
        return _epicsUtils.VectorInt_resize(self, *args)

    def insert(self, *args):
        return _epicsUtils.VectorInt_insert(self, *args)

    def reserve(self, *args):
        return _epicsUtils.VectorInt_reserve(self, *args)

    def capacity(self):
        return _epicsUtils.VectorInt_capacity(self)

    __swig_destroy__ = _epicsUtils.delete_VectorInt
    __del__ = lambda self: None


VectorInt_swigregister = _epicsUtils.VectorInt_swigregister
VectorInt_swigregister(VectorInt)


class VectorDouble(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(
        self, VectorDouble, name, value
    )
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, VectorDouble, name)
    __repr__ = _swig_repr

    def iterator(self):
        return _epicsUtils.VectorDouble_iterator(self)

    def __iter__(self):
        return self.iterator()

    def __nonzero__(self):
        return _epicsUtils.VectorDouble___nonzero__(self)

    def __bool__(self):
        return _epicsUtils.VectorDouble___bool__(self)

    def __len__(self):
        return _epicsUtils.VectorDouble___len__(self)

    def pop(self):
        return _epicsUtils.VectorDouble_pop(self)

    def __getslice__(self, *args):
        return _epicsUtils.VectorDouble___getslice__(self, *args)

    def __setslice__(self, *args):
        return _epicsUtils.VectorDouble___setslice__(self, *args)

    def __delslice__(self, *args):
        return _epicsUtils.VectorDouble___delslice__(self, *args)

    def __delitem__(self, *args):
        return _epicsUtils.VectorDouble___delitem__(self, *args)

    def __getitem__(self, *args):
        return _epicsUtils.VectorDouble___getitem__(self, *args)

    def __setitem__(self, *args):
        return _epicsUtils.VectorDouble___setitem__(self, *args)

    def append(self, *args):
        return _epicsUtils.VectorDouble_append(self, *args)

    def empty(self):
        return _epicsUtils.VectorDouble_empty(self)

    def size(self):
        return _epicsUtils.VectorDouble_size(self)

    def clear(self):
        return _epicsUtils.VectorDouble_clear(self)

    def swap(self, *args):
        return _epicsUtils.VectorDouble_swap(self, *args)

    def get_allocator(self):
        return _epicsUtils.VectorDouble_get_allocator(self)

    def begin(self):
        return _epicsUtils.VectorDouble_begin(self)

    def end(self):
        return _epicsUtils.VectorDouble_end(self)

    def rbegin(self):
        return _epicsUtils.VectorDouble_rbegin(self)

    def rend(self):
        return _epicsUtils.VectorDouble_rend(self)

    def pop_back(self):
        return _epicsUtils.VectorDouble_pop_back(self)

    def erase(self, *args):
        return _epicsUtils.VectorDouble_erase(self, *args)

    def __init__(self, *args):
        this = _epicsUtils.new_VectorDouble(*args)
        try:
            self.this.append(this)
        except:
            self.this = this

    def push_back(self, *args):
        return _epicsUtils.VectorDouble_push_back(self, *args)

    def front(self):
        return _epicsUtils.VectorDouble_front(self)

    def back(self):
        return _epicsUtils.VectorDouble_back(self)

    def assign(self, *args):
        return _epicsUtils.VectorDouble_assign(self, *args)

    def resize(self, *args):
        return _epicsUtils.VectorDouble_resize(self, *args)

    def insert(self, *args):
        return _epicsUtils.VectorDouble_insert(self, *args)

    def reserve(self, *args):
        return _epicsUtils.VectorDouble_reserve(self, *args)

    def capacity(self):
        return _epicsUtils.VectorDouble_capacity(self)

    __swig_destroy__ = _epicsUtils.delete_VectorDouble
    __del__ = lambda self: None


VectorDouble_swigregister = _epicsUtils.VectorDouble_swigregister
VectorDouble_swigregister(VectorDouble)


class VectorString(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(
        self, VectorString, name, value
    )
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, VectorString, name)
    __repr__ = _swig_repr

    def iterator(self):
        return _epicsUtils.VectorString_iterator(self)

    def __iter__(self):
        return self.iterator()

    def __nonzero__(self):
        return _epicsUtils.VectorString___nonzero__(self)

    def __bool__(self):
        return _epicsUtils.VectorString___bool__(self)

    def __len__(self):
        return _epicsUtils.VectorString___len__(self)

    def pop(self):
        return _epicsUtils.VectorString_pop(self)

    def __getslice__(self, *args):
        return _epicsUtils.VectorString___getslice__(self, *args)

    def __setslice__(self, *args):
        return _epicsUtils.VectorString___setslice__(self, *args)

    def __delslice__(self, *args):
        return _epicsUtils.VectorString___delslice__(self, *args)

    def __delitem__(self, *args):
        return _epicsUtils.VectorString___delitem__(self, *args)

    def __getitem__(self, *args):
        return _epicsUtils.VectorString___getitem__(self, *args)

    def __setitem__(self, *args):
        return _epicsUtils.VectorString___setitem__(self, *args)

    def append(self, *args):
        return _epicsUtils.VectorString_append(self, *args)

    def empty(self):
        return _epicsUtils.VectorString_empty(self)

    def size(self):
        return _epicsUtils.VectorString_size(self)

    def clear(self):
        return _epicsUtils.VectorString_clear(self)

    def swap(self, *args):
        return _epicsUtils.VectorString_swap(self, *args)

    def get_allocator(self):
        return _epicsUtils.VectorString_get_allocator(self)

    def begin(self):
        return _epicsUtils.VectorString_begin(self)

    def end(self):
        return _epicsUtils.VectorString_end(self)

    def rbegin(self):
        return _epicsUtils.VectorString_rbegin(self)

    def rend(self):
        return _epicsUtils.VectorString_rend(self)

    def pop_back(self):
        return _epicsUtils.VectorString_pop_back(self)

    def erase(self, *args):
        return _epicsUtils.VectorString_erase(self, *args)

    def __init__(self, *args):
        this = _epicsUtils.new_VectorString(*args)
        try:
            self.this.append(this)
        except:
            self.this = this

    def push_back(self, *args):
        return _epicsUtils.VectorString_push_back(self, *args)

    def front(self):
        return _epicsUtils.VectorString_front(self)

    def back(self):
        return _epicsUtils.VectorString_back(self)

    def assign(self, *args):
        return _epicsUtils.VectorString_assign(self, *args)

    def resize(self, *args):
        return _epicsUtils.VectorString_resize(self, *args)

    def insert(self, *args):
        return _epicsUtils.VectorString_insert(self, *args)

    def reserve(self, *args):
        return _epicsUtils.VectorString_reserve(self, *args)

    def capacity(self):
        return _epicsUtils.VectorString_capacity(self)

    __swig_destroy__ = _epicsUtils.delete_VectorString
    __del__ = lambda self: None


VectorString_swigregister = _epicsUtils.VectorString_swigregister
VectorString_swigregister(VectorString)


class ServerEntry(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(
        self, ServerEntry, name, value
    )
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, ServerEntry, name)
    __repr__ = _swig_repr
    __swig_setmethods__["guid"] = _epicsUtils.ServerEntry_guid_set
    __swig_getmethods__["guid"] = _epicsUtils.ServerEntry_guid_get
    if _newclass:
        guid = _swig_property(
            _epicsUtils.ServerEntry_guid_get, _epicsUtils.ServerEntry_guid_set
        )
    __swig_setmethods__["protocol"] = _epicsUtils.ServerEntry_protocol_set
    __swig_getmethods__["protocol"] = _epicsUtils.ServerEntry_protocol_get
    if _newclass:
        protocol = _swig_property(
            _epicsUtils.ServerEntry_protocol_get, _epicsUtils.ServerEntry_protocol_set
        )
    __swig_setmethods__["addresses"] = _epicsUtils.ServerEntry_addresses_set
    __swig_getmethods__["addresses"] = _epicsUtils.ServerEntry_addresses_get
    if _newclass:
        addresses = _swig_property(
            _epicsUtils.ServerEntry_addresses_get, _epicsUtils.ServerEntry_addresses_set
        )
    __swig_setmethods__["version"] = _epicsUtils.ServerEntry_version_set
    __swig_getmethods__["version"] = _epicsUtils.ServerEntry_version_get
    if _newclass:
        version = _swig_property(
            _epicsUtils.ServerEntry_version_get, _epicsUtils.ServerEntry_version_set
        )

    def __init__(self):
        this = _epicsUtils.new_ServerEntry()
        try:
            self.this.append(this)
        except:
            self.this = this

    __swig_destroy__ = _epicsUtils.delete_ServerEntry
    __del__ = lambda self: None


ServerEntry_swigregister = _epicsUtils.ServerEntry_swigregister
ServerEntry_swigregister(ServerEntry)


class epicsUtils(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, epicsUtils, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, epicsUtils, name)
    __repr__ = _swig_repr

    def __init__(self, *args):
        this = _epicsUtils.new_epicsUtils(*args)
        try:
            self.this.append(this)
        except:
            self.this = this

    __swig_destroy__ = _epicsUtils.delete_epicsUtils
    __del__ = lambda self: None
    __swig_setmethods__["pvstringlist"] = _epicsUtils.epicsUtils_pvstringlist_set
    __swig_getmethods__["pvstringlist"] = _epicsUtils.epicsUtils_pvstringlist_get
    if _newclass:
        pvstringlist = _swig_property(
            _epicsUtils.epicsUtils_pvstringlist_get,
            _epicsUtils.epicsUtils_pvstringlist_set,
        )

    def HasAlias(self, *args):
        return _epicsUtils.epicsUtils_HasAlias(self, *args)

    __swig_setmethods__["serverlist"] = _epicsUtils.epicsUtils_serverlist_set
    __swig_getmethods__["serverlist"] = _epicsUtils.epicsUtils_serverlist_get
    if _newclass:
        serverlist = _swig_property(
            _epicsUtils.epicsUtils_serverlist_get, _epicsUtils.epicsUtils_serverlist_set
        )

    def getServerList(self):
        return _epicsUtils.epicsUtils_getServerList(self)

    __swig_setmethods__["getVersion"] = _epicsUtils.epicsUtils_getVersion_set
    __swig_getmethods__["getVersion"] = _epicsUtils.epicsUtils_getVersion_get
    if _newclass:
        getVersion = _swig_property(
            _epicsUtils.epicsUtils_getVersion_get, _epicsUtils.epicsUtils_getVersion_set
        )
    __swig_setmethods__["getAddress"] = _epicsUtils.epicsUtils_getAddress_set
    __swig_getmethods__["getAddress"] = _epicsUtils.epicsUtils_getAddress_get
    if _newclass:
        getAddress = _swig_property(
            _epicsUtils.epicsUtils_getAddress_get, _epicsUtils.epicsUtils_getAddress_set
        )


epicsUtils_swigregister = _epicsUtils.epicsUtils_swigregister
epicsUtils_swigregister(epicsUtils)

# This file is compatible with both classic and new-style classes.
